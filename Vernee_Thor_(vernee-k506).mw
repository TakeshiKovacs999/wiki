{{Infobox device
| manufacturer = Vernee
| name = Thor
| codename = vernee-k506
| image = File:verne-k506.jpg
| imagecaption = pmOS splash
| releaseyear = 2016
| originalsoftware = Android 6.0
| chipset = MediaTek MT6753
| cpu = Octa-core Cortex-A53 1.3GHz
| gpu = Mali-T720 MP3
| storage = 16GB
| display = 5" IPS 720x1280
| memory = 3GB
| architecture = aarch64
<!-- the following status_* questions should be answered with Y - yes, P - partial, N - no, or left blank (for untested or unknown) -->
| status_usbnet = Y
| status_flashing = P
| status_touch = Y
| status_screen = Y
| status_wifi = N
| status_xwayland = N
| status_fde = 
| status_mainline = N
| status_battery = 
| status_3d = N
| status_accel = 
| status_audio = N
| status_bluetooth = N
| status_camera = N
| status_gps = N
| status_mobiledata = N
| status_sms = N
| status_calls = N
| status = 
| booting = yes
| pmoskernel = 3.18.99
<!-- you can also use these lines if you need to:
if the device haven't originally ran Android OS, e.g. Nokia N900
| n-android =  ✔
see Unixbench page on wiki
| whet_dhry = 0.0
Is OTG available, not used in wiki
| status_otg = -
-->
}}

== Contributors ==
* [[user:Dahrkael|Dahrkael]]

== Maintainer(s) ==
<!-- Only if this device doesn't run on linux-postmarketos yet! -->

== Users owning this device ==
{{Device owners}}
<!-- autogenerated, use {{Owns device|devicepage|notes}} on your profile page -->
<!-- use _ instead of spaces in device page name, e.g. {{Owns device|HTC_Desire_(htc-bravo)|custom notes}}-->
<!-- you may need to purge page cache to see changes (more->purge cache)-->
<!-- you can use {{My devices}} on your profile page to show table with all your devices -->

== How to enter flash mode ==
* '''Recovery:''' Power on the device with both {{Button|Power}}  and {{Button|Volume Up}} buttons. Then select ''Recovery'' pressing {{Button|Volume Up}} and confirm with {{Button|Volume Down}}.
* '''Fastboot:''' Do the same as for Recovery, but select the ''Fastboot'' option in the menu.
* '''MediaTek Preloader:''' The preloader triggers automatically everytime the phone turns on or reboots.

== Installation ==
Fastboot doesn't allow flashing on this phone.

pmOS can be installed either using a ''Recovery ZIP'' through '''TWRP''' or using MediaTek's '''SP Flash Tool'''.

=== Unlock the bootloader ===
# Enable Developer Mode in Android tapping 5 times the <code>Build Number</code> in <code>Settings -> About Phone</code>.
# Then go to <code>Settings -> Developer Options</code> and tap <code>OEM Unlock</code>.
=== Install using Recovery ZIP ===
Follow the [[Installation_from_recovery_mode|Installation from recovery mode]] guide.
=== Install using SP Flash Tool ===
# Export the pmOS files running <code>pmbootstrap install && pmbootstrap export</code>
# Open '''SP Flash Tool'''.
# Select the [https://pastebin.com/aUfY5kPk MT6753 scatter file].
# Asign <code>boot.img-vernee-k506</code> to partition '''boot''' and <code>vernee-k506.img</code> to partition '''system'''
# Click {{Button|Download}} and turn on/restart the phone. It will automatically flash the files.
== See also ==
* [https://www.needrom.com/download/twrp-thor/ TWRP 3.0.2 ROM for Vernee Thor]

<!--
Link to related GitLab issues or merge requests like the following (replace CHANGEME with the ID, e.g. 1234):
* {{MR|CHANGEME|pmaports}} Initial merge request
* {{issue|CHANGEME|pmaports}} Some related issue
-->
<!--
If you manage to get your device packages merged, uncomment links below and change DEVICE_CODENAME with the actual one.
* {{Device package|vernee-k506}}
* {{Kernel package|vernee-k506}}
-->
